package com.example.rvalencia.tipcalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    double bill = 00.00;
    int numberOfGuests;
    int tipPercentage;
    double tipRatio;
    double totalCost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        final EditText billTotal = (EditText)findViewById(R.id.txtBill);
        final Spinner guestGroup = (Spinner)findViewById(R.id.txtGroup);
        final Spinner tipGroup = (Spinner)findViewById(R.id.txtGroup2);
        Button cost = (Button)findViewById(R.id.btnCalculate);
        cost.setOnClickListener(new View.OnClickListener() {
            final TextView result = ((TextView)findViewById(R.id.txtResult));
            @Override
            public void onClick(View v) {
                bill = Double.parseDouble(billTotal.getText( ).toString( ));
                numberOfGuests = Integer.parseInt(guestGroup.getSelectedItem( ).toString( ));
                tipPercentage = Integer.parseInt(tipGroup.getSelectedItem( ).toString( ).replace("%", ""));
                if (tipPercentage == 0) {
                    tipRatio = 1.00;
                } else if (tipPercentage == 5) {
                    tipRatio = 1.05;
                } else if (tipPercentage == 10) {
                    tipRatio = 1.10;
                } else if (tipPercentage == 15) {
                    tipRatio = 1.15;
                } else if (tipPercentage == 20) {
                    tipRatio = 1.20;
                } else if (tipPercentage == 25) {
                    tipRatio = 1.25;
                }
                totalCost = (bill * tipRatio) / numberOfGuests;
                DecimalFormat currency = new DecimalFormat("$###,###.##");
                result.setText("Cost for each of " + numberOfGuests + " guests: " + NumberFormat.getCurrencyInstance(new Locale("en", "US")).format(totalCost));
            }
        });
    }
}
