# LIS4331 - Advanced Mobile Applications Development

## Rene Valencia

### Project 2 Requirements:

_Deliverables:_  
1. Include splash screen  
2. Insert at least five sample tasks (see p.413)  
3. Test database class (see pp. 424, 425)  
4. Must add background color(s) or theme   
5. Create and display launcher icon image    

Bitbucket repo links:  
    a) https://bitbucket.org/rv17h/lis4331/src/master/  
    b) https://bitbucket.org/rv17h/lis4331/src/master/p2  

README.md file should include the following items:
    * Screenshot of running application’s Task List (optional splash screen: 10 additional pts.) 
    
> "This is a blockquote.
> 
> This is the second paragraph in the block quote.
> 

    
Assignment Screenshots:

First run | Second run
:-------------------------:|:-------------------------:
![](img/1.png)  |  ![](img/2.png)

    
    