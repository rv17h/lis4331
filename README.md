# LIS4331 - Advanced Mobile Applications Development

## Rene Valencia

### LIS4331 Requirements:

_Course Work Links:_

1. [A1 README.md](https://bitbucket.org/rv17h/lis4331/src/master/a1/README.md)
	* Install JDK
	* Install Android Studio and create My First App and Contacts App
	* Provide screenshots of installations
	* Create Bitbucket repo
	* Complete Bitbucket tutorial (bitbucketstationlocations)
	* Provide git command descriptions
2. [A2 README.md](https://bitbucket.org/rv17h/lis4331/src/master/a2/README.md)
	* Create Split The Bill app
	* Provide screenshots (unpopulated and populated)
3. [A3 README.md](https://bitbucket.org/rv17h/lis4331/src/master/a3/README.md)
	* Create Currency Converter app
	* Provide screenshots (unpopulated, data validation, and populated)
4. [A4 README.md](https://bitbucket.org/rv17h/lis4331/src/master/a4/README.md)
	* Create Mortgage Interest Calculator app
	* Provide screenshots (splash screen, invalid, and valid)
5. [A5 README.md](https://bitbucket.org/rv17h/lis4331/src/master/a5/README.md)
	* Create News Reader app
	* Provide screenshots
6. [P1 README.md](https://bitbucket.org/rv17h/lis4331/src/master/p1/README.md)
	* Create My Music app
	* Provide screenshots
7. [P2 README.md](https://bitbucket.org/rv17h/lis4331/src/master/p2/README.md)
	* Create Task List app  
	* Provide screenshots  