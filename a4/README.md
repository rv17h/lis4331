# LIS4331 - Advanced Mobile Applications Development

## Rene Valencia

### Assignment 4 Requirements:

_Deliverables:_  
1. Include splash screen image, app title, intro text.  
2. Include appropriate images.  
3. Must use persistent data: SharedPreferences  
4. Widgets and images must be vertically and horizontally aligned.  
5. Must add background color(s) or theme  
6. Create and display launcher icon image  

Bitbucket repo links:  
    a) https://bitbucket.org/rv17h/lis4331/src/master/  
    b) https://bitbucket.org/rv17h/lis4331/src/master/a4

README.md file should include the following items:
    * Screenshot of running application’s splash screen
    * Screenshot of running application’s invalid screen
    * Screenshots of running application’s valid screen
    
> "This is a blockquote.
> 
> This is the second paragraph in the block quote.
> 

    
Assignment Screenshots:

Splash Screen | Unpopulated Screen
:-------------------------:|:-------------------------:
![](img/1.png)  |  ![](img/2.png)

Invalid Screen | Valid Screen
:-------------------------:|:-------------------------:
![](img/3.png)  |  ![](img/4.png)
    
    