# LIS4331 - Advanced Mobile Applications Development

## Rene Valencia

### Assignment 5 Requirements:

_Deliverables:_  
1. Include splash screen image with app title and list of articles  
2. Must find and use your own RSS Feed  
3. Must add background color(s) or theme   
4. Create and display launcher icon image    

Bitbucket repo links:  
    a) https://bitbucket.org/rv17h/lis4331/src/master/  
    b) https://bitbucket.org/rv17h/lis4331/src/master/a5  

README.md file should include the following items:
    * Screenshot of running application’s splash screen (list of articles - activity_items.xml);
    * Screenshot of running application’s individual article (activity_item.xml);  
    * Screenshots of running application’s default browser (article link);  
    
> "This is a blockquote.
> 
> This is the second paragraph in the block quote.
> 

    
Assignment Screenshots:

Items activity | Item activity
:-------------------------:|:-------------------------:
![](img/1.png)  |  ![](img/2.png)

Read more... |
:-------------------------:|:-------------------------:
![](img/3.png)  |  
    
    