# LIS4331 - Advanced Mobile Applications Development

## Rene Valencia

### Assignment 1 Requirements:

_Four Parts:_

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Questions
4. Bitbucket repo links:  

    a) https://bitbucket.org/rv17h/lis4331/src/master/  
    
    b) https://bitbucket.org/rv17h/bitbucketstationlocations/src/master/

README.md file should include the following items:
    * Screenshot of a1 application running
    * git commands w/ short descriptions
    
> "This is a blockquote.
> 
> This is the second paragraph in the block quote.
> 
> Git commands w/ short descriptions:"

    1. git init - creates a new git repository
    2. git status - get the status of local repository or file
    3. git add - tells git to track new files
    4. git commit - takes staging changes and commits them to project history
    5. git push - pushes commited changes to repository
    6. git pull - pulls changes from remote repository into local repository
    7. git clone - clones a repository
    
Assignment Screenshots:

Hello.java             |  My First App
:-------------------------:|:-------------------------:
![](img/1.png)  |  ![](img/2.png)
    
Contact App             |  Contact App
:-------------------------:|:-------------------------:
![](img/3.png)  |  ![](img/4.png)

Contact App             |  Contact App
:-------------------------:|:-------------------------:
![](img/5.png)  |  ![](img/6.png)