# LIS4331 - Advanced Mobile Applications Development

## Rene Valencia

### Assignment 3 Requirements:

_Deliverables:_

1. Field to enter U.S. dollar amount: 1–100,000
2. Must include toast notification if user enters out-of-range values
3. Radio buttons to convert from U.S. to Euros, Pesos and Canadian currency (must be vertically and horizontally aligned)
4. Must include correct sign for euros, pesos, and Canadian dollars
5. Must add background color(s) or theme
6. Create and display launcher icon image  

Bitbucket repo links:  
    a) https://bitbucket.org/rv17h/lis4331/src/master/  
    b) https://bitbucket.org/rv17h/lis4331/src/master/a3

README.md file should include the following items:
    * Screenshot of running application’s unpopulated user interface
    * Screenshot of running application’s toast notification
    * Screenshots of running application’s converted currency user interface
    
> "This is a blockquote.
> 
> This is the second paragraph in the block quote.
> 

    
Assignment Screenshots:

Currency Converter (unpopulated) | Currency Converter (data validation with toast message)
:-------------------------:|:-------------------------:
![](img/1.png)  |  ![](img/2.png)

Currency Converter (US to Euros) | Currency Converter (US to Pesos)
:-------------------------:|:-------------------------:
![](img/3.png)  |  ![](img/4.png)

Currency Converter (US to Canadian) |
:-------------------------:|:-------------------------:
![](img/5.png)  |
    
    