package com.example.rvalencia.currencyconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {
    double euroConversionRate = 0.89;
    double pesoConversionRate = 19.24;
    double canadianConversionRate = 1.32;
    double dollarsEntered;
    double convertedDollars;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        final EditText dollars = (EditText)findViewById(R.id.txtDollars);
        final RadioButton usToEuros = (RadioButton)findViewById(R.id.radUsToEuros);
        final RadioButton usToPesos = (RadioButton)findViewById(R.id.radUsToPesos);
        final RadioButton usToCanadian = (RadioButton)findViewById(R.id.radUsToCanadian);
        final TextView result = (TextView)findViewById(R.id.txtResult);
        Button convert = (Button)findViewById(R.id.btnConvert);

        dollars.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                dollars.setHint(hasFocus ? "" : "My hint");
            }
        });

        convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dollarsEntered = Double.parseDouble(dollars.getText().toString());
                DecimalFormat tenth = new DecimalFormat("#.#");
                if (usToEuros.isChecked()) {
                    if (dollarsEntered <=100000 && dollarsEntered > 0) {
                        convertedDollars = dollarsEntered * euroConversionRate;
                        result.setText("€" + tenth.format(convertedDollars) + " Euros");
                    } else {
                        Toast.makeText(MainActivity.this,"US Dollars must be <= 100,000 and > 0", Toast.LENGTH_LONG).show();
                    }
                }
                if (usToPesos.isChecked()) {
                    if (dollarsEntered <=100000 && dollarsEntered > 0) {
                        convertedDollars = dollarsEntered * pesoConversionRate;
                        result.setText("MEX$" + tenth.format(convertedDollars) + " Pesos");
                    } else {
                        Toast.makeText(MainActivity.this,"US Dollars must be <= 100,000 and > 0", Toast.LENGTH_LONG).show();
                    }
                }
                if (usToCanadian.isChecked()) {
                    if (dollarsEntered <=100000 && dollarsEntered > 0) {
                        convertedDollars = dollarsEntered * canadianConversionRate;
                        result.setText("C$" + tenth.format(convertedDollars) + " Canadian");
                    } else {
                        Toast.makeText(MainActivity.this,"US Dollars must be <= 100,000 and > 0", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

    }
}
