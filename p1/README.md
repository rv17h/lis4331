# LIS4331 - Advanced Mobile Applications Development

## Rene Valencia

### Project 1 Requirements:

_Deliverables:_

1. Include splash screen image, app title, intro text.
2. Include artists' images and media.
3. Images and buttons must be vertically and horizontally aligned.
4. Must add background color(s) or theme.
5. Create and display launcher icon image.  

Bitbucket repo links:  
    a) https://bitbucket.org/rv17h/lis4331/src/master/  
    b) https://bitbucket.org/rv17h/lis4331/src/master/p1

README.md file should include the following items:
    * Screenshot of running application’s splash screen
    * Screenshot of running application’s follow-up screen (with images and buttons)
    * Screenshots of running application’s play and pause user interfaces (with images and buttons)
    
> "This is a blockquote.
> 
> This is the second paragraph in the block quote.
> 

    
Assignment Screenshots:

My Music (splash screen) | My Music (follow-up screen)
:-------------------------:|:-------------------------:
![](img/1.png)  |  ![](img/2.png)

My Music (playing first artist) | My Music (playing second artist)
:-------------------------:|:-------------------------:
![](img/3.png)  |  ![](img/4.png)

My Music (playing third artist) |
:-------------------------:|:-------------------------:
![](img/5.png)  |
    
    