package com.example.rvalencia.mymusic;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button button1, button2, button3;
    MediaPlayer mpDiamondOrtiz, mpMidnightNorth, mpOtis;
    int playing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        button1 = (Button) findViewById(R.id.btnDiamondOrtiz);
        button2 = (Button) findViewById(R.id.btnMidnightNorth);
        button3 = (Button) findViewById(R.id.btnOtis);

        button1.setOnClickListener(bDiamondOrtiz);
        button2.setOnClickListener(bMidnightNorth);
        button3.setOnClickListener(bOtis);

        mpDiamondOrtiz = new MediaPlayer();
        mpDiamondOrtiz = MediaPlayer.create(this, R.raw.diamondortiz);

        mpMidnightNorth = new MediaPlayer();
        mpMidnightNorth = MediaPlayer.create(this, R.raw.midnightnorth);

        mpOtis = new MediaPlayer();
        mpOtis = MediaPlayer.create(this, R.raw.otis);
        playing = 0;
    }

    Button.OnClickListener bDiamondOrtiz = new Button.OnClickListener() {
    @Override
    public void onClick(View v) {
        switch (playing) {
            case 0:
                mpDiamondOrtiz.start();
                playing = 1;
                button1.setText("Pause Diamond Ortiz");
                button2.setVisibility(View.INVISIBLE);
                button3.setVisibility(View.INVISIBLE);
                break;
            case 1:
                mpDiamondOrtiz.pause();
                playing = 0;
                button1.setText("Play Diamond Ortiz");
                button2.setVisibility(View.VISIBLE);
                button3.setVisibility(View.VISIBLE);
                break;
        }

    }
    };

    Button.OnClickListener bMidnightNorth = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (playing) {
                case 0:
                    mpMidnightNorth.start();
                    playing = 1;
                    button2.setText("Pause Midnight North");
                    button1.setVisibility(View.INVISIBLE);
                    button3.setVisibility(View.INVISIBLE);
                    break;
                case 1:
                    mpMidnightNorth.pause();
                    playing = 0;
                    button2.setText("Play Midnight North");
                    button1.setVisibility(View.VISIBLE);
                    button3.setVisibility(View.VISIBLE);
                    break;
            }

        }
    };
    Button.OnClickListener bOtis = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (playing) {
                case 0:
                    mpOtis.start();
                    playing = 1;
                    button3.setText("Pause Otis McDonald");
                    button1.setVisibility(View.INVISIBLE);
                    button2.setVisibility(View.INVISIBLE);
                    break;
                case 1:
                    mpOtis.pause();
                    playing = 0;
                    button3.setText("Play Otis McDonald");
                    button1.setVisibility(View.VISIBLE);
                    button2.setVisibility(View.VISIBLE);
                    break;
            }

        }
    };
}
