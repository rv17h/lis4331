import java.util.Scanner;
import java.util.ArrayList;

public class NestedStructures {
  public static void main(String [] args) {
    System.out.print("Program counts, totals, and averages total number of user-entered exam scores."
    + "\nPlease enter exam scores between 0 and 100, inclusive."
    + "\nEnter out of range number to end program."
    + "\nMust *only* permit numeric entry.\n\n");

    Scanner sc = new Scanner(System.in);
    ArrayList<Double> inputs =  new ArrayList<Double>();
    int count = 0;
    double input;
    double total = 0;
    
    while (true){
      System.out.print("Enter exam score: ");
      if (sc.hasNextDouble()){
        input = sc.nextDouble();
        if (input >= 0 && input <=100){
          count++;
          inputs.add(input);
        }
        else {
          break;
        }
      }
      else{
        sc.next();
      }
      System.out.print("Not a valid number!");
      System.out.print("\n\nPlease try again. Enter exam score: ");
      if (sc.hasNextInt()){
        input = sc.nextDouble();
        if (input >= 0 && input <=100){
          count++;
          inputs.add(input);
        }
        else {
          break;
        }
      }
      else{
        sc.next();
      }
    }
    
    for (double i : inputs){
      total += i;
    }
    // double total = input;
    double average = total / count;

    System.out.print("Count: " + count);
    System.out.print("\nTotal: " + total);
    System.out.print("\nAverage: " + average);
    System.out.println();
    sc.close();
  }
}