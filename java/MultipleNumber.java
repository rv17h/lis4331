import java.util.Scanner;

public class MultipleNumber
{
   public static void main(String args[])
   {
    Scanner sc = new Scanner(System.in);
    System.out.print("Program determines if first number is multiple of second, prints result.\nExample: 2, 4, 6, 8 are multiples of 2.\n1) Use integers. 2) Use printf() function to print.\nMust *only* permit integer entry.");
    
    System.out.print("\n\nNum1: ");
    while (!sc.hasNextInt()) {
      System.out.print("Not a valid number!");
      System.out.print("\n\nPlease try again. Enter Num1: ");
      sc.next();
    }
    int num1 = sc.nextInt();

    System.out.print("\nNum2: ");
    while (!sc.hasNextInt()) {
      System.out.print("Not a valid number!");
      System.out.print("\n\nPlease try again. Enter Num2: ");
      sc.next();
    }
    int num2 = sc.nextInt();

    int product = num1 / num2;
    if (num1 % num2 == 0) {
      System.out.print("\n\n" + num1 + " is a multiple of " + num2);
      System.out.print("\nThe product of " + product + " and " + num2 + " is " + num1);
    }
    sc.close();
    }
  }