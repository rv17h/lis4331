import java.util.Scanner;

public class MeasurementConversion {
  public static void main(String [] args) {
    System.out.print("Program converts inches to centimeters, meters, feet, yards, and miles."
    + "\n***Notes***:"
    + "\n1) Use integer for inches (must validate integer input)."
    + "\n2) Use printf() function to print (format values per below output)."
    + "\n3) Create Java \"constants\" for the following values:"
    + "\n\tINCHES_TO_CENTIMETER,"
    + "\n\tINCHES_TO_METER,"
    + "\n\tINCHES_TO_FOOT,"
    + "\n\tINCHES_TO_YARD,"
    + "\n\tFEET_TO_MILE\n\n");

    Scanner sc = new Scanner(System.in);

    System.out.print("Please enter number of inches: ");
    while (!sc.hasNextInt()){
        System.out.print("Not a valid number!");
        System.out.print("\n\nPlease enter number of inches: ");
        sc.next();
    }
    
    int inches = sc.nextInt();
    double INCHES_TO_CENTIMETER = 2.54;
    double INCHES_TO_METER = 0.0254;
    double INCHES_TO_FOOT = 12;
    double INCHES_TO_YARD = 36;
    double FEET_TO_MILE = 5280;

    System.out.printf("%,d", inches);
    System.out.print(" inch(es) equals\n");
    System.out.printf("\n%,.6f", (inches * INCHES_TO_CENTIMETER));
    System.out.print(" centimeter(s)");
    System.out.printf("\n%,.6f", (inches * INCHES_TO_METER));
    System.out.print(" meter(s)");
    System.out.printf("\n%,.6f", (inches / INCHES_TO_FOOT));
    System.out.print(" feet");
    System.out.printf("\n%,.6f", (inches / INCHES_TO_YARD));
    System.out.print(" yard(s)");
    System.out.printf("\n%,.8f", ((inches / INCHES_TO_FOOT) / FEET_TO_MILE));
    System.out.print(" mile(s)");
    System.out.println();
    sc.close();
  }
}