import java.util.Scanner;
import java.text.DecimalFormat;

class BookDemo {
  public static void main(String[] args) {
    String cd = "";
    String dc = "";
    double pr = 0;
    String au = "";
    Scanner sc = new Scanner(System.in);

    DecimalFormat f = new DecimalFormat("$###,###.##");

    System.out.println("\n/////Below are *base class default constructor* values (instantiating p1, then using getter methods)://///");
    Product p1 = new Product(); // create default object
    System.out.println("\nCode = " + p1.getCode());
    System.out.println("Description = " + p1.getDescription());
    System.out.println("Price = " + f.format(p1.getPrice()));

    System.out.println("\n/////Below are *base class* user-entered values (instatiating p2, then using getter methods)://///");
    
    // get user input
    System.out.print("\nCode: ");
    cd = sc.nextLine();

    System.out.print("Description: ");
    dc = sc.nextLine();

    System.out.print("Price: ");
    pr = sc.nextDouble();

    Product p2 = new Product(cd, dc, pr);
    System.out.println("\nCode = " + p2.getCode());
    System.out.println("Description = " + p2.getDescription());
    System.out.println("Price = " + f.format(p2.getPrice()));

    System.out.println("\n/////Below using setter methods to pass literal values to p2, then print() method to display values://///");
    p2.setCode("xyz789");
    p2.setDescription("Test Widget");
    p2.setPrice(89.99);
    p2.print();

    System.out.println();

    System.out.println("\n/////Below are *derived class default constructor* values (instantiating b1, then using getter methods)://///");
    Book b1 = new Book();
    System.out.println("\nCode = " + b1.getCode());
    System.out.println("Description = " + b1.getDescription());
    System.out.println("Price = " + f.format(b1.getPrice()));
    System.out.println("Author = " + b1.getAuthor());

    System.out.println("\nOr using overridden derived class print() method...");
    b1.print();

    System.out.println();

    System.out.println("\n/////Below are *derived class* user-entered values (instantiating b2, then using getter methods)://///");
    // get user input
    System.out.print("\nCode: ");
    cd = sc.next();

    System.out.print("Description: ");
    sc.nextLine();
    dc = sc.nextLine();

    System.out.print("Price: ");
    pr = sc.nextDouble();

    System.out.print("Author: ");
    sc.nextLine();
    au = sc.nextLine();

    Book b2 = new Book(cd, dc, pr, au);
    System.out.println("\nCode = " + b2.getCode());
    System.out.println("Description = " + b2.getDescription());
    System.out.println("Price = " + f.format(b2.getPrice()));
    System.out.println("Author = " + b2.getAuthor());

    System.out.println("\nOr using derived class print() method...");
    b2.print();

    System.out.println();
    sc.close();
  }
}