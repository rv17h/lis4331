import java.util.Scanner;

public class Circle
{
   public static void main(String args[])
   {
      Scanner sc = new Scanner(System.in);
      System.out.print("Non-OOP program calculates diameter, circumference, and circle area.\nMust use Java's built-in PI constant, printf(), and formatted to 2 decimal places.\nMust *only* permit numeric entry.");
      
      System.out.print("\n\nEnter the radius: ");
      while (!sc.hasNextDouble()) {
        System.out.print("Not a valid number!");
        System.out.print("\n\nPlease try again. Enter the radius: ");
        sc.next();
        }
        double radius = sc.nextDouble();
        //Diameter = radius*2
        double diameter = 2*radius;
        System.out.printf("\nCircle diameter: %.2f", diameter);
        //Circumference = 2*PI*radius
        double circumference= Math.PI * 2*radius;
        System.out.printf("\nCircumference: %.2f", circumference);
        //Area = PI*radius*radius
        double area = Math.PI * (radius * radius);
        System.out.printf("\nArea: %.2f", area);
        sc.close();
      }
  }