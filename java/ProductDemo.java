import java.util.Scanner;
import java.text.DecimalFormat;

class ProductDemo {
  public static void main(String[] args) {
    String cd = "";
    String dc = "";
    double pr = 0;
    Scanner sc = new Scanner(System.in);

    DecimalFormat f = new DecimalFormat("$###,###.##");

    System.out.println("\n/////Below are default constructor values://///");
    Product v1 = new Product(); // create default object
    System.out.println("\nCode = " + v1.getCode());
    System.out.println("Description = " + v1.getDescription());
    System.out.println("Price = " + f.format(v1.getPrice()));

    System.out.println("\n/////Below are user-entered values://///");

    // get user input
    System.out.print("\nCode: ");
    cd = sc.nextLine();

    System.out.print("Description: ");
    dc = sc.nextLine();

    System.out.print("Price: ");
    pr = sc.nextDouble();

    Product v2 = new Product(cd, dc, pr);
    System.out.println("\nCode = " + v2.getCode());
    System.out.println("Description = " + v2.getDescription());
    System.out.println("Price = " + f.format(v2.getPrice()));

    System.out.println("\n/////Below using setter methods to pass literal values, then print() method to display values://///");
    v2.setCode("xyz789");
    v2.setDescription("Test Widget");
    v2.setPrice(89.99);
    v2.print();
    sc.close();

  }
}