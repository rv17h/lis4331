import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;


public class MultipleSelectionLists extends JFrame {
    private JList jList;
    private JList jListForCopy;
    private JButton copyButton;
    private static final String[] listItems = { "Cuban Cigar", "Liberty Bell", "Gator Hater",
            "Mac Daddy", "Lip Smacker", "Build Your Own", "Sushi Chef", "Shepherd's Pie", "Mexecutioner", "Oh Canada" };

    public MultipleSelectionLists() {
        super("Multiple Selection Lists");
        setLayout(new FlowLayout());

        jList = new JList(listItems);
        jList.setFixedCellHeight(15);
        jList.setFixedCellWidth(140);
        jList.setVisibleRowCount(5);
        jList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        add(new JScrollPane(jList));
        
        copyButton = new JButton("Copy >>>");
        
        copyButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                jListForCopy.setListData(jList.getSelectedValues());
            }
        });
        
        add(copyButton);
        jListForCopy = new JList();
        jListForCopy.setFixedCellHeight(15);
        jListForCopy.setFixedCellWidth(140);
        jListForCopy.setVisibleRowCount(5);
        jListForCopy.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        add(new JScrollPane(jListForCopy));
    }
    public static void main(String[] args) {
        MultipleSelectionLists jListDemo = new MultipleSelectionLists();
        jListDemo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jListDemo.setSize(450, 120);
        jListDemo.setVisible(true);
    }
}