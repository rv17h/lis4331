import java.io.*;
import java.util.Scanner;

public class WriteReadFile {
  public static void main(String [] args) {

    // Name of file
    String fileName = "filewrite.txt";

    // This will reference one line at a time
    String line = null;

    try {
      // Scanner
      Scanner sc = new Scanner(System.in);
      // User input
      System.out.print("Enter text: ");
      line = sc.nextLine();
      
      // Writing file
      PrintWriter writer = new PrintWriter(fileName, "UTF-8");
      writer.println(line);
      writer.close();
      System.out.println("\nCreated " + fileName + ".");

      // Reading text file
      FileReader fileReader = new FileReader(fileName);

      // Wrapping FileReader in BufferedReader
      BufferedReader bufferedReader = new BufferedReader(fileReader);

      System.out.println();
      System.out.println(bufferedReader.readLine());

      // while((line = bufferedReader.readLine()) != null) {
      //   String[] splitted = line.split("\\,");
      //   for (String part : splitted) {
      //       System.out.println(part);
      //   }  
      //   // System.out.println(line);
      // }   

      // Closing file and Scanner
      bufferedReader.close();
      sc.close();         
    }

    catch(FileNotFoundException ex) {
        System.out.println(
            "Unable to open file '" + 
            fileName + "'");                
    }
    
    catch(IOException ex) {
        System.out.println(
            "Error reading file '" 
            + fileName + "'");
    }
  }
}
