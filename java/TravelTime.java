import java.util.Scanner;

public class TravelTime {
  public static void main(String[] args) {
    double input;
    double miles;
    double mph;
    boolean stop = false;
    Scanner sc = new Scanner(System.in);

    System.out.print("Program calculates approximate travel time.\n\n");

    while (!stop) {
      // miles
      while (true) {
        System.out.print("Please enter miles: ");
        if (sc.hasNextDouble()) {
          input = sc.nextDouble();
          if (input > 0 && input < 3000) {
            miles = input;
            break;
          }
          else {
            System.out.print("Miles must be greater than 0, and no more than 3000.\n\n");
          }
        }
        else {
          System.out.print("Invalid double--miles must be a number.\n\n");
          sc.next();
        }
      }

      // MPH
      while (true) {
        System.out.print("Please enter MPH: ");
        if (sc.hasNextDouble()) {
          input = sc.nextDouble();
          if (input > 0 && input < 100) {
            mph = input;
            break;
          }
          else {
            System.out.print("MPH must be greater than 0, and no more than 100.\n\n");
          }
        }
        else {
          System.out.print("Invalid double--MPH must be a number.\n\n");
          sc.next();
        }
      }
      
      // Solve for time: time = distance / speed
      Double travelTime = miles / mph;

      // Format hours and minutes
      int hour = travelTime.intValue();
      Long minutes = Math.round((travelTime - hour) * 60);
      System.out.print("\nEstimated travel time: " + hour + " hr(s) " + minutes + " minutes");

      // Ask user if they want to continue or not
      System.out.print("\n\nContinue? (y/n): ");
      String s = sc.next();

      if(s.equals("n")) {
          stop = true;
          sc.close();
      }
    } 
  }
}