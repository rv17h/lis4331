import java.text.DecimalFormat;

class Book extends Product {
  private String author;

  DecimalFormat f = new DecimalFormat("$###,###.##");

  public Book() {
    super();
    System.out.println("\nInside book default constructor.");
    author = "John Doe";
  }

  public Book(String code, String description, double price, String author) {
    super(code, description, price);
    System.out.println("\nInside book constructor with parameters.");
    this.author = author;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String au) {
    author = au;
  }

  public void print() {
    super.print();
    System.out.print(", Author: " + author);
  }
}